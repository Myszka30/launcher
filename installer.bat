@echo off
where git >nul 2>nul
if %errorlevel%==1 (
   	@echo Git not found in path.
	@echo Attempting to install git using winget.
	winget install Git.Git  >nul
	@echo Git installed
)

echo Succesfully found Git!
cd ./Forge
git init
git remote add origin https://gitlab.com/bolekmcdev/ModpackRepo.git
git pull origin main --allow-unrelated-histories

del username.txt
set /P username="Wpisz swoja nazwe uzytkownika: "
echo %username%>>username.txt

echo Succesfully installed the modpack
pause
exit